// btrie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "index.h"
#include "ntx.h"


void LoadFile(INDEX_SYSTEM *nidx)
{
	char key[512];
	printf("\n  Please enter file to read :");
	scanf("%s", key);

#ifndef _DEBUG	
	FILE *fl = fopen(key, "r");
#else
	FILE *fl = fopen("C:\a1.index", "r");
	//D:\\drive_I\\Hexagon\\projects\\btrie\\btrie\\B.001
#endif
	if (fl != 0)
	{
		char rec[1000];
		long recno = 1;
		while(!feof(fl))
		{
			memset(rec, 0, 1000);
			fgets(rec, 990, fl);
			int l = strlen(rec);
			if (rec[l-1]=='\n') rec[l-1] = 0;
			nidx->AddIndexKey(rec, recno); 
			if (!(recno%200)) printf("Record :%d\r", recno);
			recno++;
		}
		fclose(fl);
		printf("File Loaded\n");
	}
	else
		{
			printf("File not loaded\n");
		}
}

void SearchFor(INDEX_SYSTEM *nidx)
{
	char key[500];
	printf("\n  Please enter key to search :");
	scanf("%s", key);

	int rno = nidx->SearchFor(key);
	printf("Record Number :%d\n", rno);
}

void DeleteFor(INDEX_SYSTEM *nidx)
{
	char key[500];
	printf("\n  Please enter key to delete :");
	scanf("%s", key);

	if (nidx->DeleteKey(key)) printf("Key Found and deleted\n");
	else printf("Key Not Found\n");
}

int _tmain(int argc, _TCHAR* argv[])
{
	INDEX_SYSTEM *nidx = new INDEX_SYSTEM;
    nidx->Create();
	while(-1)
	{
		printf("---------------------------------------------\n"); 
		printf("1. Open\n");
		printf("2. Delete\n");
		printf("3. Search\n");
		printf("4. Quit\n");
		printf("\n  Please enter your option :");
		switch(getchar())
		{
		case '1':LoadFile(nidx); break;
		case '2':DeleteFor(nidx); break;
		case '3':SearchFor(nidx); break;
		case '4':delete nidx; return 0;
		}
	}
	return 0;
}

