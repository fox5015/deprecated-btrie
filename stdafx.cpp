// stdafx.cpp : source file that includes just the standard includes
// btrie.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"
#include <io.h>


long FileLength(int handle)                   { return _filelength(handle);        }
long LSeek(int fd, long offset, int whence)   { return _lseek(fd, offset, whence); }
long _Read(int fd, void *buffer, int size)    { return _read(fd, buffer, size);    }
long _Write(int fd, void *buffer, int size)   { return _write(fd, buffer, size);   }
void _ChSize(int fd, long size)               { _chsize(fd, size);                 }
void _Close(int fd)                           { _close(fd);                        }
int _Open(const char *filename, int oflag, int pmode)  {  return _open(filename, oflag, pmode);  }

int Explode(std::vector<std::string> &out, const char *in, const char *seperator, int minCount)
{
	out.clear();
	std::string t = in;
    int p = t.find(seperator); 
	while(p != std::string::npos)
	{
		out.push_back(t.substr(0, p) + seperator);
		t = t.substr(p+strlen(seperator));
		p = t.find(seperator); 
	}
	if (t.length() > 0) out.push_back(t);

	return out.size();
}

int ExplodeX(std::vector<std::string> &out, const char *in)
{
	std::vector<std::string> out1, out2;
	int i, z = Explode(out, in, "/");

	for ( i = 0 ; i < z ; i++)
	{
		Explode(out2, out[i].c_str(), "_");
		out1.insert(out1.end(), out2.begin(), out2.end());
	}
	out = out1;
	return out.size();
}
