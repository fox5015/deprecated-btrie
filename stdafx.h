// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <share.h>
#include <string>
#include <vector>
#include <sstream>
#include <istream>
#include <algorithm>


#define __WINDOWS__

#define KEYSIZE      4
//#define PAGESIZE     512
//#define ITEMCOUNT    41

#define UNIQUE         0
#define DUPLICATE      1

#define OFF            0
#define ON             1

#define _APPEND        0
#define _EDIT          1
#define _DELETE        2
#define _ENQUIRY       4

#define READ_RECORD    1
#define SEARCH_RECORD  2

#define V_STRING    1
#define V_VOID      9
/////////////////////////////////////////////////////////////////////////////////

long FileLength(int handle);
long LSeek(int fd, long offset, int whence);
long _Read(int fd, void *buffer, int size);
long _Write(int fd, void *buffer, int size);
void _ChSize(int fd, long size);
void _Close(int fd);
int _Open(const char *filename, int oflag, int pmode);

int Explode(std::vector<std::string> &out, const char *in, const char *seperator, int minCount=-1);
int ExplodeX(std::vector<std::string> &out, const char *in);

