#ifndef __INDEXSYSTEM__
#define __INDEXSYSTEM__

#include "ntx_page.h"
/*
class Pattern
{
public:
	int id;
	std::string p;

	Pattern() { id=0; }
	bool operator ==(Pattern &t)  {  return (p == t.p);  }
	bool operator < (Pattern &t)  {  return (p < t.p);   }
	bool operator > (Pattern &t)  {  return (p > t.p);   }
};
*/
class NODE_ADR {
public:
   int	    address;
   short    key;
   void operator=(NODE_ADR &T)  { memcpy(this, &T, sizeof(NODE_ADR)); }
};

//------------------------------------------------------------------------------------

class BOOK_MARK
{
   NODE_ADR      node[30];
   int			 recordNo;
   std::string   keyParts[30];
public:
    int           kCount;
    char			 cl;
    char          Level;

    BOOK_MARK()                        {  cl = Level = 0;  }
    ~BOOK_MARK()                       {  Reset();                           }

    void      operator=(BOOK_MARK &T);
    int       Add(long addr,int keyIndex);
    void      Reset(void);

    NODE_ADR& operator[](int t)        { return node[t];  }
    NODE_ADR& operator()(int t=-1)     { return node[(t==-1) ? cl : t]; }

    int       KeyOffset(int t=-1)      { return ((t==-1) ? cl : t) * KEYSIZE; }
	int RNo()                         { return (cl == kCount-1) ? recordNo : 0; }

    const char *GetKey(int t=-1)        
    { 
	   if (t==-1) return keyParts[cl].c_str();
	   return keyParts[t].c_str(); 
    }
    void SetKey(char *expression, int m);   
};

//------------------------------------------------------------------------------------

class INDEX_SYSTEM {
protected:
	int AddKeyInParts(char *expr, _NTX_PAGE *page1, int lvl, long *pageno);
   int  AddKeyAtLevel(int);   
   void AddKeyAtPosition(_NTX_PAGE *,int lvl);
   long ReadPage(_NTX_PAGE **page1,long offset);

   void NewRoot(long,_NTX_PAGE *,_NTX_PAGE *);
	int ShareWithSibling(_NTX_PAGE **page1, int lvl, int rno);
   int  BreakPage(int,_NTX_PAGE **,_NTX_PAGE *, int);

// VIRTUAL FUNCTIONS {
   virtual void DeleteKey(int lvl)                                  {            }
   virtual void DeleteKey(unsigned char *expression)                {            }
   virtual int  PrintPage(long no)                                  { return 0;  }
   void PrintHeader(void);
   long Search(long offset=-1);
// } VIRTUAL FUNCTIONS

   void InitSearch(char *expression=0, int rno=0);
	_NTX_PAGE *AppendPage(int *pos);

   struct {
       long           root;
       unsigned char  allowDuplicate;
   }header;

protected:

   BOOK_MARK Link;
   _NTX_PAGE *ntxPages[400000];
   int       ntxPageCount;


public:
   INDEX_SYSTEM()                                                         { memset(this,0,sizeof(INDEX_SYSTEM)); }
   ~INDEX_SYSTEM();                                                         
   void Create(int un=0);
   long GetKeyCount(void);                              
	int  DeleteKey(const char *exp);
    int   SearchFor(const char *exp);
    void  Close(void);
    void  PrintKeys(int tp=0);
    void  AddIndexKey(char *expr,int recno);
};

#endif
//------------------------------------------------------------------------------------

