#include "stdafx.h"
#include "index.h"
#include <stdlib.h>

void BOOK_MARK::operator=(BOOK_MARK &T)
{
   Reset();
   Level=T.Level;
   memcpy(node,T.node,100*sizeof(NODE_ADR));
}

void BOOK_MARK::Reset(void)
{
   Level=0; 
   cl = 0;
}

int BOOK_MARK::Add(long addr,int keyIndex)
{
	cl = Level;
    node[Level].address=addr;
    node[Level].key=(char)keyIndex;
	Level++;
    return (Level - 1);
}

//================================================================================

INDEX_SYSTEM::~INDEX_SYSTEM()
{
   //patCount = 0;
	Close();
	header.allowDuplicate = 0;
}

void BOOK_MARK::SetKey(char *expression, int m)   
{ 
	recordNo=m; 
	std::vector<std::string> out;
	ExplodeX(out, expression);
	for (int i = 0 ; i < (int)out.size() ; i++) keyParts[i] = out[i];
	kCount = out.size();
}

void INDEX_SYSTEM::InitSearch(char *expression, int rno)
{
    Link.SetKey(expression, rno);
    Link.Reset();
}

_NTX_PAGE *INDEX_SYSTEM::AppendPage(int *pos)
{
   _NTX_PAGE *Blk1 = new _NTX_PAGE(40);
   if (pos != 0) (*pos) = ntxPageCount;

   if (ntxPageCount >= 399950)
   {
	   int distribution[140];
	   memset(distribution, 0, 140 * sizeof(int));
	   int mc=0, m1=0, m2=9999, c1=0, c2=0;
	   for (int i = 1 ; i < ntxPageCount ; i++) 
	   {
		   int y = ntxPages[i]->GetCount();
		   c2 += ntxPages[i]->GetPageSize();
		   distribution[y]++;
		   mc += y;
		   if (y > m1) m1 = y;
		   if (y < m2) m2 = y;
		   if (y==0) c1++;
	   }

	   mc /= ntxPageCount;
	   int u=0;
   }
   Blk1->SetPageID(ntxPageCount);
   ntxPages[ntxPageCount++] = Blk1;
   return Blk1;
}

void INDEX_SYSTEM::Close(void)
{
   Link.Reset();
   for (int i = 0 ; i < ntxPageCount ; i++) delete ntxPages[i];
}

long INDEX_SYSTEM::GetKeyCount(void)
{
   long tot=0;
   for (int i = 0 ; i < ntxPageCount ; i++) 
   {
       tot += ntxPages[i]->GetRecordCount();
   }
   return tot;
}

//=================================================================================
void INDEX_SYSTEM::Create(int un)
{
    memset(&header,0,sizeof(header));
    header.root=0; 
	header.allowDuplicate = un;
    AppendPage(0);
}

//---------------------------------------------------------------------------------
int INDEX_SYSTEM::AddKeyInParts(char *expr, _NTX_PAGE *page1, int lvl, long *pageno)
{
	int pos;
	while(-1)
    {
		char *ex = &expr[lvl*KEYSIZE];
		NODE_ADR na = Link(lvl);
		_NTX_PAGE *Blk1 = AppendPage(&pos);

		if (lvl==-1) header.root = pos;
		else
		{
			page1->GetItem(na.key);
			page1->item->page = pos;
		}
	    lvl = Link.Add(pos, 0);
	    page1 = Blk1;

	    AddKeyAtPosition(page1,lvl);
    }
}

int INDEX_SYSTEM::ShareWithSibling(_NTX_PAGE **page10, int lvl, int rno)
{
	_NTX_PAGE *page1 = (*page10);
    int nl = page1->GetNextLink();
    if (nl==0) return 0;

    _NTX_PAGE *page3=0;
    ReadPage(&page3, nl);
    if (page3->IsFull()) return 0;

	int x = 0;
	int remaining = (page3->GetItemCount() - page3->GetCount()) / 2;
	if (remaining==0) return 0;

	page3->MoveBy(page1, remaining);
	if (Link[lvl].key > (int)page1->GetCount()) 
	{
		Link[lvl].key -= page1->GetCount();
		page3->Move(Link[lvl].key,Link[lvl].key+1);
		page3->Add(Link[lvl].key, Link.GetKey(lvl), rno); 
	    (*page10) = page3;
	}
	else 
	{
		page1->Move(Link[lvl].key,Link[lvl].key+1);
        page1->Add(Link[lvl].key, Link.GetKey(lvl), rno); 
	}
	return 1;
}

int INDEX_SYSTEM::AddKeyAtLevel(int lvl)
{
   _NTX_PAGE *page1 = 0;
   int pos = 0;
   NODE_ADR na = Link();

   ReadPage(&page1, Link().address);

   if (page1->IsFull())
   {
	   if (page1->GetPageSize()==40) page1->ExpandTo(80);
	   else if (page1->GetPageSize()==80) page1->ExpandTo(128);
	   else if (page1->GetPageSize()==128) page1->ExpandTo(256);
	   else if (page1->GetPageSize()==256) page1->ExpandTo(512);
	   else if (page1->GetPageSize()==512) page1->ExpandTo(768);
	   else if (page1->GetPageSize()==768) page1->ExpandTo(1024);
	   else if (page1->GetPageSize()==1024) page1->ExpandTo(1280);
	   else if (page1->GetPageSize()==1280) page1->ExpandTo(1536);
   }

   if (!page1->IsFull()) 
   {
	   const char *ex = Link.GetKey();
	   while(Link.cl < Link.kCount)
	   {
	       AddKeyAtPosition(page1,-1);

			if (Link.cl < Link.kCount-1)
		    {
				_NTX_PAGE *Blk1 = AppendPage(&pos);
				page1->GetItem(Link().key);
				page1->item->page = pos;
         
				lvl = Link.Add(pos, 0);
				page1 = Blk1;
			}
			else break;
	   }
       return 0;
   }

    _NTX_PAGE *page2 = 0;
	int rno = Link.RNo();
	if (ShareWithSibling(&page1, lvl, rno)==0)
	{
	    int nl = page1->GetNextLink();
		_NTX_PAGE *page2 = AppendPage(&pos);
		page2->ExpandTo(page1->GetPageSize());

		page1->SetNextLink(pos);
		page2->SetPrevLink(na.address);
		page2->SetNextLink(nl);
		if (nl > 0) 
		{
			_NTX_PAGE *page3 = 0;
			ReadPage(&page3, nl);
			page3->SetPrevLink(pos);
	    } 

		BreakPage(lvl,&page1,page2, rno);
   }

	if (rno == 0)
	{
		do
		{
			na = Link(lvl);
			_NTX_PAGE *Blk1 = AppendPage(&pos);
			page1->GetItem(na.key);
			page1->item->page = pos;

			lvl = Link.Add(pos, 0);

			page1 = Blk1;
			AddKeyAtPosition(page1,lvl);
		}while(strlen(Link.GetKey(lvl)) > KEYSIZE);
    }
    return 0;
}

//----------------------------------------------------------------------------------

void INDEX_SYSTEM::AddKeyAtPosition(_NTX_PAGE *page1,int lvl)
{
	int rno = Link.RNo();
	page1->Move(Link(lvl).key,Link(lvl).key+1);
	page1->Add(Link(lvl).key,Link.GetKey(lvl), rno);
	int dummy = 0;
}

//----------------------------------------------------------------------------------

void INDEX_SYSTEM::AddIndexKey(char *expression,int rec_no)
{
   InitSearch(expression, rec_no);
   int f = Search();
   if (!f || header.allowDuplicate)
   {
	   AddKeyAtLevel(Link.cl);
   }
   else
   {
	    NODE_ADR na = Link();
		_NTX_PAGE *page3=0;
		ReadPage(&page3, na.address);
		page3->GetItem(na.key);
		page3->item->rec_no = rec_no;
   }
}

//=================================================================================

void INDEX_SYSTEM::PrintKeys(int tp)
{
   int i;
   long j,k;
//   PrintHeader();
   switch(tp) {
       case 0:for(i=0;i<15;i++) if (Link[i].address!=0) PrintPage(Link[i].address); break;
       case 1:k=0;
	      for(j=1;j<k;j++) if (!PrintPage(j)) break;
   }
}

//----------------------------------------------------------------------------------

long INDEX_SYSTEM::ReadPage(_NTX_PAGE **page1,long offset)
{
   int page_offset = offset;
   if (offset==-1) page_offset=header.root;

   if (page_offset < 0 || page_offset >= ntxPageCount) return -1;
   (*page1) = ntxPages[page_offset];
   return page_offset;
}

//----------------------------------------------------------------------------------

long INDEX_SYSTEM::Search(long offset)
{
   long         page_offset;
   _NTX_PAGE    *page1;

   if ((page_offset=ReadPage(&page1,offset))==-1) return -1;

   int pos=-1;
   int found = page1->SearchFor(Link.GetKey(Link.Level), &pos);

   while(!found && pos == page1->GetCount() && page1->GetNextLink() > 0)
   {
	   int nl = page1->GetNextLink();
		page_offset = ReadPage(&page1, nl);
		found = page1->SearchFor(Link.GetKey(), &pos);
   }

   Link.Add(page_offset, pos);
   if (found)
   {
	   if (Link.cl < Link.kCount-1) 
		{
			page1->GetItem(pos);
			return Search(page1->item->page);
		}
		return 1;
   }
   return 0;
}

//=================================================================================

int INDEX_SYSTEM::BreakPage(int lvl,_NTX_PAGE **page10,_NTX_PAGE *page2, int rno)
{
	_NTX_PAGE *page1 = *page10;
    int add=0;
    int t = page2->GetItemCount() / 2;
	page2->MoveBy(page1, t);
    if (Link[lvl].key > (int)page1->GetCount()) 
    {
       Link[lvl].key -= page1->GetCount();
       page2->Move(Link[lvl].key,Link[lvl].key+1);
	   page2->Add(Link[lvl].key, Link.GetKey(lvl), rno); 
	   (*page10) = page2;
       return 2;
    }
    page1->Move(Link[lvl].key,Link[lvl].key+1);
	page1->Add(Link[lvl].key, Link.GetKey(lvl), rno); 
    return 1;
}

//----------------------------------------------------------------------------------

void INDEX_SYSTEM::NewRoot(long ad2,_NTX_PAGE *page1,_NTX_PAGE *page2)
{
// Setup New root

// Get Last entry of first page
   page1->GetItem(page1->GetCount()-1);
   page1->AddToCount(-1);
   //Write(Link[0].address,page1);

// Copy Item to root
   page2->GetItem(0);
   //memcpy(page2->item,page1->item,header.item_size);
   page2->item->page=Link[0].address;

   page2->GetItem(1);
   page2->item->page=ad2;
   page2->SetCount(1);

// Get address of new root
   //pno=GetFreePage();
   //Write(pno,page2);
   header.root=1;
}

void INDEX_SYSTEM::PrintHeader(void)
{
   _NTX_PAGE    *page1;
   FILE         *fl;

   fl=fopen("HEADER","w");
   fprintf(fl,"ROOT PAGE    :%ld\n",header.root);

   if (header.root) {
       page1=new _NTX_PAGE;
       //Read(header.root,page1);
       //fprintf(fl,"OFFSET       :%d\n",page1->GetOffset(0));
       delete page1;
   }
   fclose(fl);
}

int INDEX_SYSTEM::SearchFor(const char *exp)
{
    InitSearch((char *)exp, 0);
    if (Search())
	{
		NODE_ADR na = Link();
		_NTX_PAGE *page3=0;
		ReadPage(&page3, na.address);
		page3->GetItem(na.key);
		return page3->item->rec_no;
	}
	return 0;
}

int INDEX_SYSTEM::DeleteKey(const char *exp)
{
    InitSearch((char *)exp, 0);
    if (Search())
	{
		NODE_ADR na = Link();
		_NTX_PAGE *page3=0;
		ReadPage(&page3, na.address);
		page3->GetItem(na.key);
		page3->item->rec_no = 0;
		return 1;
	}
	return 0;
}
