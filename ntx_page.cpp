#include "stdafx.h"
#include "ntx_page.h"

_NTX_PAGE::_NTX_PAGE(int sz)
{
	Page.Array = new char[sz];
	Page.Item = (PData *)Page.Array;

	Page.Item->pageSize = sz;
	Page.Item->itemCount = (sz - 16) / sizeof(_ITEM);
	Clear();
}

_NTX_PAGE::~_NTX_PAGE()
{
	for ( int i = 0 ; i < GetCount() ; i++) Page.Item->items[i].Free();
	delete Page.Array;
}

void _NTX_PAGE::ExpandTo(int newSz)
{
	char *temp = Page.Array;
	int oldSize = Page.Item->pageSize;

	Page.Array = new char[newSz];
	memcpy(Page.Array, temp, oldSize);
	delete temp;

	Page.Item = (PData *)Page.Array;

	Page.Item->pageSize = newSz;
	Page.Item->itemCount = (newSz - 16) / sizeof(_ITEM);
}

void _NTX_PAGE::GetItem(int t)          
{ 
	item = 0;  
	if (t < Page.Item->itemCount) item = &Page.Item->items[t];  
}

int  _NTX_PAGE::GetCount()              
{ 
	return Page.Item->count;         
}

void _NTX_PAGE::Clear()                 
{ 
	Page.Item->pageID = Page.Item->next = Page.Item->prev = 0;
	Page.Item->count = 0;
	memset(Page.Item->items, 0, Page.Item->itemCount * sizeof(_ITEM));
}

void _NTX_PAGE::SetNextLink(int y)      
{ 
	Page.Item->next = y;     
}

void _NTX_PAGE::SetPrevLink(int y)      
{ 
	Page.Item->prev = y;     
}

int _NTX_PAGE::GetNextLink()            
{ 
	return Page.Item->next;     
}

int _NTX_PAGE::GetPrevLink()            
{ 
	return Page.Item->prev;     
}

void _NTX_PAGE::SetCount(int r)         
{ 
	Page.Item->count = r;    
}

void _NTX_PAGE::AddToCount(int r)       
{ 
	Page.Item->count += r;   
}

void _NTX_PAGE::Move(int from,int too)
{
   if (too > from) memmove(&Page.Item->items[too], &Page.Item->items[from], (Page.Item->itemCount-too) * sizeof(_ITEM));
   if (too < from) memmove(&Page.Item->items[too], &Page.Item->items[from], (Page.Item->itemCount-from) * sizeof(_ITEM));
}

void _NTX_PAGE::MoveBy(_NTX_PAGE *page1, int c)
{
   memmove(&Page.Item->items[c], &Page.Item->items[0], (Page.Item->count) * sizeof(_ITEM));

   // Lets copy last c items from page1 to this page
   int y = page1->GetCount() - c;
   memcpy(&Page.Item->items[0], &page1->Page.Item->items[y], c*sizeof(_ITEM));
   page1->AddToCount(-c);
   AddToCount(c);
}

int _NTX_PAGE::SearchFor(const char *exp, int *position)
{
	int m = -1;
	int start = 0;
	int end = GetCount()-1;

	if (end==-1) { (*position) = 0; return 0; }

    int ans = strcmp(Page.Item->items[start].key, exp);
	if (ans > 0) { (*position) = start; return 0; }
	if (ans==0) { (*position) = start; return 1; }

    ans = strcmp(Page.Item->items[end].key, exp);
	if (ans < 0) { (*position) = end+1; return 0; }
	if (ans==0) { (*position) = end; return 1; }

	while(start < end) 
	{
		m = (start + end) / 2;
		ans = strcmp(Page.Item->items[m].key, exp);
		if (m==start) break;

		if (ans > 0) end = m;
		else if (ans < 0) start = m;
		else if (ans==0) { (*position) = m; return 1; }
	}
	(*position) = start;
	if (m==start)
	{
		if (ans < 0) { (*position) = end; return 0; }
		if (ans > 0) { (*position) = start; return 0; }
	}
	return 0;
}

void _NTX_PAGE::Add(int at, const char *r, int recno)
{
	GetItem(at);
    if (recno > 0) item->rec_no = recno;
    item->page = 0;
    item->key = _strdup(r);
	Page.Item->count++;   
}

int _NTX_PAGE::GetRecordCount()
{
	int i, tot = 0;
	for ( i = 0 ; i < GetCount() ; i++) if (Page.Item->items[i].rec_no > 0) tot++;
	return tot;
}

//----------------------------------------------------------------------------------

