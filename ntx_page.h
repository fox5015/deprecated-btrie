#ifndef ___NTX_PAGE__
#define ___NTX_PAGE__

class _ITEM {
public:
   long page;
   long rec_no;
   char *key;

   _ITEM()      { key = 0; page = rec_no = 0; }
   ~_ITEM()     {    }
   void Free()  { if (key != 0) delete key;   }
};

//------------------------------------------------------------------------------------

typedef struct __pdata__
{
	int      pageID;
	short    pageSize;
	char     itemCount;
	char     count;
	int      next;
	int      prev;
	_ITEM    items[1];
}PData;

class _NTX_PAGE 
{
	union 
	{
	    char         *Array;
		PData        *Item;
	}Page;
public:
	_ITEM        *item;

	_NTX_PAGE(int sz=40);
	~_NTX_PAGE();
	void GetItem(int t);
	int  GetCount();  
	int  IsFull()     {  return (GetCount() < Page.Item->itemCount) ? 0 : 1;  }
	int  HalfPage()   {  return Page.Item->itemCount / 2; }
	int GetPageSize() {  return Page.Item->pageSize; }
	int GetItemCount(){  return Page.Item->itemCount; }

	void Clear();                 

	void SetNextLink(int y);      
	void SetPrevLink(int y);      

	int GetNextLink();            
	int GetPrevLink();            

	void SetCount(int r);         
    void AddToCount(int r);       
    void Move(int from,int too);
	void MoveBy(_NTX_PAGE *page1, int too);
	void Add(int at, const char *r, int recno);

	int GetRecordCount();
	void ExpandTo(int newSz);

	void SetPageID(int id)                    
	{ 
		Page.Item->pageID = id; 
	}
	int SearchFor(const char *exp, int *position);

};

#endif
