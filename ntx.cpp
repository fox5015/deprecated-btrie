#include "stdafx.h"
#include "index.h"
#include "ntx.h"

_NTX::_NTX() : INDEX_SYSTEM()
{
}

//=================================================================================

//=================================================================================

int _NTX::PrintPage(long offset)
{
   long         page_offset;
   _NTX_PAGE    *page1;
   unsigned int i;
   char         fl_name[10];
   FILE         *fl;

   page_offset=(offset==-1) ? header.root : offset;
   if (!(page1=new _NTX_PAGE)) return 0;

   //Read(page_offset,page1);
   sprintf(fl_name,"PAGE%04d",page_offset/page1->GetPageSize());
   fl=fopen(fl_name,"w");

   //fprintf(fl,"KEYSIZE=%d, ROOT=%ld, COUNT=%d\n",header.key_size, header.root / 1024l, page1->GetCount());

   for(i=0;i<page1->GetCount();i++) {
       page1->GetItem(i);
       //fprintf(fl,"KEY:%-*.*s  RECORD:%8ld - PAGE :(%8ld - %8ld)\n",header.key_size,header.key_size,page1->item->key,page1->item->rec_no,page1->item->page, page1->item->page/1024l);
   }
   page1->GetItem(i);
   //fprintf(fl,"KEY:%-*.*s                  - PAGE :(%8ld - %8ld)\n",header.key_size,header.key_size,"",page1->item->page, page1->item->page/1024l);
   fclose(fl);
   delete page1;
   return 1;
}

